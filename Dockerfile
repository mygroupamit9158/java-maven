# Stage 1: Build the Java application using Maven

FROM maven:latest as builder
WORKDIR /app
COPY pom.xml .
RUN mvn dependency:go-offline
COPY src ./src
RUN mvn clean install -DskipTests

# Stage 2: Create the final Docker image with the Java application

FROM tomcat:latest
RUN mkdir -p /mnt/airasia/failedcases/savetoDB/litmus/
COPY --from=builder /app/target/*.jar /usr/local/tomcat/webapps/ROOT.jar
EXPOSE 8080
CMD ["catalina.sh", "run"]

# install basic vim editor for checking logs if there is any issue
RUN apt update -y
RUN apt install vim -y

# install Database application
RUN apt-get install -y postgresql
EXPOSE 5432
